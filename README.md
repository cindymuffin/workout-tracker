# Workout Tracker

## About

A minimalistic workout calendar to keep track of your workouts, exercises, and proud achievements! Details can be added to each exercise for your planned workout, such as your target reps and sets with weight, duration, or whatever detail you would like to add, and each exercise can be updated to show what you actually accomplished after finishing your workout. You can also keep track of any proud achievements made in your workouts!

## Web Application Link

Access Workout Tracker here:
https://cal-workout.herokuapp.com/accounts/login/

