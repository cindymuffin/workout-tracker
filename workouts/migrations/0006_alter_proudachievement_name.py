# Generated by Django 4.0.3 on 2022-04-07 00:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('workouts', '0005_alter_workout_date_proudachievement'),
    ]

    operations = [
        migrations.AlterField(
            model_name='proudachievement',
            name='name',
            field=models.CharField(max_length=150, verbose_name='Exercise'),
        ),
    ]
