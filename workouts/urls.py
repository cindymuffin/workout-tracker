from django.urls import path
from workouts.views import (
    WorkoutListView,
    WorkoutDetailView,
    WorkoutCreateView,
    WorkoutUpdateView,
    WorkoutDeleteView,
    ProudAchievementListView,
    ProudAchievementCreateView,
    ProudAchievementUpdateView,
)


urlpatterns = [
    path("", WorkoutListView.as_view(), name="list_workouts"),
    path("<int:pk>/", WorkoutDetailView.as_view(), name="show_workout"),
    path("create/", WorkoutCreateView.as_view(), name="create_workout"),
    path("<int:pk>/edit/", WorkoutUpdateView.as_view(), name="edit_workout"),
    path(
        "<int:pk>/delete/", WorkoutDeleteView.as_view(), name="delete_workout"
    ),
    path(
        "achievements/",
        ProudAchievementListView.as_view(),
        name="list_achievements",
    ),
    path(
        "create_achievement/",
        ProudAchievementCreateView.as_view(),
        name="create_achievement",
    ),
    path(
        "<int:pk>/edit_achievement/",
        ProudAchievementUpdateView.as_view(),
        name="edit_achievement",
    ),
]
