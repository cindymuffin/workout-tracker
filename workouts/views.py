from workouts.models import Workout, ProudAchievement
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import (
    CreateView,
    UpdateView,
    DeleteView,
)
from django.utils.safestring import mark_safe
from django.shortcuts import redirect
from django.urls import reverse_lazy
from .utils import Calendar, get_date, prev_month, next_month
from bootstrap_datepicker_plus.widgets import DatePickerInput


class WorkoutListView(LoginRequiredMixin, ListView):
    model = Workout
    template_name = "workouts/list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        d = get_date(self.request.GET.get("month", None))
        cal = Calendar(d.year, d.month)
        html_cal = cal.formatmonth(self.request.user, withyear=True)
        context["calendar"] = mark_safe(html_cal)
        context["prev_month"] = prev_month(d)
        context["next_month"] = next_month(d)
        return context

    def get_queryset(self):
        d = get_date(self.request.GET.get("month", None))
        return Workout.objects.filter(
            owner=self.request.user,
            date__year=d.year,
            date__month=d.month,
        ).order_by("date")


class WorkoutDetailView(LoginRequiredMixin, DetailView):
    model = Workout
    template_name = "workouts/detail.html"


class WorkoutCreateView(LoginRequiredMixin, CreateView):
    model = Workout
    template_name = "workouts/create.html"
    fields = ["name", "description", "date"]

    def get_form(self):
        form = super().get_form()
        form.fields["date"].widget = DatePickerInput()
        return form

    def form_valid(self, form):
        workout = form.save(commit=False)
        workout.owner = self.request.user
        workout.save()
        return redirect("show_workout", pk=workout.id)


class WorkoutUpdateView(LoginRequiredMixin, UpdateView):
    model = Workout
    template_name = "workouts/edit.html"
    fields = ["name", "description", "date"]

    def get_form(self):
        form = super().get_form()
        form.fields["date"].widget = DatePickerInput()
        return form

    def form_valid(self, form):
        workout = form.save(commit=False)
        workout.owner = self.request.user
        workout.save()
        return redirect("show_workout", pk=workout.id)


class WorkoutDeleteView(LoginRequiredMixin, DeleteView):
    model = Workout
    template_name = "workouts/delete.html"
    success_url = reverse_lazy("list_workouts")


class ProudAchievementListView(LoginRequiredMixin, ListView):
    model = ProudAchievement
    template_name = "workouts/achievement_list.html"

    def get_queryset(self):
        return ProudAchievement.objects.filter(
            owner=self.request.user
        ).order_by("date")


class ProudAchievementCreateView(LoginRequiredMixin, CreateView):
    model = ProudAchievement
    template_name = "workouts/achievement_create.html"
    fields = ["name", "achievement", "date"]

    def get_form(self):
        form = super().get_form()
        form.fields["date"].widget = DatePickerInput()
        return form

    def form_valid(self, form):
        achievement = form.save(commit=False)
        achievement.owner = self.request.user
        achievement.save()
        return redirect("list_achievements")


class ProudAchievementUpdateView(LoginRequiredMixin, UpdateView):
    model = ProudAchievement
    template_name = "workouts/achievement_edit.html"
    fields = ["name", "achievement", "date"]

    def get_form(self):
        form = super().get_form()
        form.fields["date"].widget = DatePickerInput()
        return form

    def get_success_url(self):
        return reverse_lazy("list_achievements")

    def form_valid(self, form):
        if "confirm_delete" in self.request.POST:
            pk = form.instance.pk
            post_delete = ProudAchievement.objects.get(pk=pk)
            post_delete.delete()
        else:
            achievement = form.save(commit=False)
            achievement.owner = self.request.user
            achievement.save()
        return redirect("list_achievements")
