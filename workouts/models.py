from django.db import models
from django.conf import settings


class Workout(models.Model):
    name = models.CharField("Workout", max_length=150)
    description = models.CharField(max_length=200, blank=True, null=True)
    date = models.DateField()
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="workouts",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name


class ProudAchievement(models.Model):
    name = models.CharField("Exercise", max_length=150)
    achievement = models.CharField(max_length=200)
    date = models.DateField()
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="achievements",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name
