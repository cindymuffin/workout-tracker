import calendar
from datetime import date, datetime, timedelta
from calendar import HTMLCalendar

from django.urls import reverse_lazy
from workouts.models import Workout, ProudAchievement
from django.templatetags.static import static


class Calendar(HTMLCalendar):
    def __init__(self, year=None, month=None):
        self.year = year
        self.month = month
        super(Calendar, self).__init__()

    # formats a day as a td
    # filter events by day
    def formatday(self, day, workouts, achievement):
        workouts_per_day = workouts.filter(date__day=day)
        d = ""
        for workout in workouts_per_day:
            d += f'<li class="nav_item"><a href="{reverse_lazy("show_workout", args=[workout.id])}">{workout.name}</a></li>'

        achievement_per_day = achievement.filter(date__day=day)
        award_img = ""
        if len(achievement_per_day) > 0:
            award_img = f"<a href='{reverse_lazy('list_achievements')}'><img class='award' src='{static('images/award.png')}' /></a>"

        if day != 0:
            return f"<td class='workout-calendar-column'><span class='date'>{day}</span> {award_img} <ul class='cal_nav'> {d} </ul></td>"
        return "<td></td>"

    # formats a week as a tr
    def formatweek(self, theweek, workouts, achievement):
        week = ""
        for d, weekday in theweek:
            week += self.formatday(d, workouts, achievement)
        return f"<tr> {week} </tr>"

    # formats a month as a table
    # filter events by year and month
    def formatmonth(self, owner, withyear=True):
        workouts = Workout.objects.filter(
            owner=owner,
            date__year=self.year,
            date__month=self.month,
        )
        achievement = ProudAchievement.objects.filter(
            owner=owner,
            date__year=self.year,
            date__month=self.month,
        )

        cal = f'<table border="0" cellpadding="0" cellspacing="0" class="design">\n'
        cal += f"{self.formatmonthname(self.year, self.month, withyear=withyear)}\n"
        cal += f"{self.formatweekheader()}\n"
        for week in self.monthdays2calendar(self.year, self.month):
            cal += f"{self.formatweek(week, workouts, achievement)}\n"
        cal += f"</table>"
        return cal


def get_date(req_day):
    if req_day:
        year, month = (int(x) for x in req_day.split("-"))
        return date(year, month, day=1)
    return datetime.today()


def prev_month(d):
    first = d.replace(day=1)
    prev_month = first - timedelta(days=1)
    month = "month=" + str(prev_month.year) + "-" + str(prev_month.month)
    return month


def next_month(d):
    days_in_month = calendar.monthrange(d.year, d.month)[1]
    last = d.replace(day=days_in_month)
    next_month = last + timedelta(days=1)
    month = "month=" + str(next_month.year) + "-" + str(next_month.month)
    return month
