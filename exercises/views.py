from exercises.models import Exercise
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import CreateView, UpdateView
from django.urls import reverse_lazy
from django.shortcuts import redirect

from workouts.models import Workout


class ExerciseCreateView(LoginRequiredMixin, CreateView):
    model = Exercise
    template_name = "exercises/create.html"
    fields = ["name", "target", "workout"]
    
    def get_form(self):
        form = super().get_form()
        form.fields["workout"].queryset = Workout.objects.filter(owner=self.request.user)
        return form

    def get_success_url(self):
        return reverse_lazy("show_workout", args=[self.object.workout_id])


class AccomplishedUpdateView(LoginRequiredMixin, UpdateView):
    model = Exercise
    template_name = "exercises/update.html"
    fields = ["accomplished"]

    def get_success_url(self):
        return reverse_lazy("show_workout", args=[self.object.workout_id])


class ExerciseUpdateView(LoginRequiredMixin, UpdateView):
    model = Exercise
    template_name = "exercises/edit.html"
    fields = ["name", "target", "workout"]

    def get_success_url(self):
        return reverse_lazy("show_workout", args=[self.object.workout_id])

    def form_valid(self, form):
        if "confirm_delete" in self.request.POST:
            pk = form.instance.pk
            post_delete = Exercise.objects.get(pk=pk)
            post_delete.delete()
        else:
            exercise = form.save(commit=False)
            exercise.save()
        return redirect("show_workout", pk=self.object.workout_id)
