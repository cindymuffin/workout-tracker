from django.urls import path
from exercises.views import (
    ExerciseCreateView,
    ExerciseUpdateView,
    AccomplishedUpdateView,
)


urlpatterns = [
    path("create/", ExerciseCreateView.as_view(), name="create_exercise"),
    path(
        "<int:pk>/edit/",
        ExerciseUpdateView.as_view(),
        name="edit_exercise",
    ),
    path(
        "<int:pk>/update/",
        AccomplishedUpdateView.as_view(),
        name="update_exercise",
    ),
]
