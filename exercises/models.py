from django.db import models


class Exercise(models.Model):
    name = models.CharField("Exercise", max_length=200)
    target = models.CharField(
        "Target Goal", max_length=200, blank=True, null=True
    )
    accomplished = models.CharField(max_length=200, blank=True, null=True)
    workout = models.ForeignKey(
        "workouts.Workout", related_name="exercises", on_delete=models.CASCADE
    )

    def __str__(self):
        return self.name
